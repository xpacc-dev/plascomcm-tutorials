# LaTeX Makefile
PROJECT=plascomcm_tutorial
TEX=pdflatex
%BIBTEX=bibtex

BUILDTEX=$(TEX) $(PROJECT).tex

all:
	$(BUILDTEX)
	$(BUILDTEX)

clean:
	rm -f *.dvi *.log *.bak *.aux *.bbl *.blg *.idx *.ps *.eps *.toc *.out *~
